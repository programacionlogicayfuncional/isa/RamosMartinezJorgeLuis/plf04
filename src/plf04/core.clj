(ns plf04.core)

(defn string-e-1
  [s]
  (letfn [(g [c m]
             (cond 
               (and (= c \e) (>= (m :c) 3)) {:r false :c (inc (m :c))}
               (and (= c \e) (>= (m :c) 0)) {:r true :c (inc (m :c))}
               :else m))
          (f [xs]
             (if (empty? xs)
               {:r false :c 0}
               (g (first xs) (f (rest xs)))))]
    ((f s) :r)))

(string-e-1 "Hello")
(string-e-1 "Heelle")
(string-e-1 "Heelele")
(string-e-1 "Hll")
(string-e-1 "e")
(string-e-1 "")

(defn string-e-2
  [s]
  (letfn [(g [x]
             (and (>= x 1) (<= x 3)))
          (h [x y]
             (if (= x \e) (inc y) y))
          (f [xs acc]
             (if (empty? xs)
               (g acc)
               (f (rest xs)
                  (h (first xs) acc))))]
    (f s 0)))

(string-e-2 "Hello")
(string-e-2 "Heelle")
(string-e-2 "Heelele")
(string-e-2 "Hll")
(string-e-2 "e")
(string-e-2 "")

(let [xs ["Hello" "Heelle" "Heelele" "Hll" "e" ""]]
  (map (fn [x] {:x x
                :se1 (string-e-1 x)
                :se2 (string-e-2 x)})
       xs))

(let [xs ["Hello" (list \H \e \l \l \o) (vector \H \e \l \l \o) (hash-set \H \e \l \l \o)]]
  (map (fn [x] {:x x
                :se1 (string-e-1 x)
                :se2 (string-e-2 x)})
       xs))

(defn string-times-1
  [s n]
  (letfn [(g [ys]
             (apply str ys))
          (f [xs x]
             (if (zero? x)
               (g [])
               (g (concat xs (f xs (dec x))))))]
    (f s n)))

(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Ho boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)

(defn string-times-2
  [xs x]
  (letfn [(g [ys]
             (apply str ys))
          (f [ys y acc]
            (if (= y 0)
              (g acc)
              (g (f ys (dec y) (concat acc ys)))))]
    (f xs x [])))


(string-times-2 "Hi" 2)
(string-times-2 "Hi" 3)
(string-times-2 "Hi" 1)
(string-times-2 "Hi" 0)
(string-times-2 "Hi" 5)
(string-times-2 "Ho boy!" 2)
(string-times-2 "x" 4)
(string-times-2 "" 4)
(string-times-2 "code" 2)
(string-times-2 "code" 3)

(defn front-times-1
  [xs x]
  (letfn [(g [ys]
             (apply str ys))
          (f [ys y]
            (if (= y 0)
              (g [])
              (g (concat (take 3 ys) (f ys (dec y))))))]
    (f xs x)))

(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "A" 4)
(front-times-1 "Ab" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)

(defn front-times-2
  [xs x]
  (letfn [(g [ys]
             (apply str ys))
          (f [ys y acc]
            (if (= y 0)
              (g acc)
              (g (f ys (dec y) (concat acc (take 3 ys))))))]
    (f xs x [])))

(front-times-2 "Chocolate" 2)
(front-times-2 "Chocolate" 3)
(front-times-2 "Abc" 3)
(front-times-2 "Ab" 4)
(front-times-2 "" 4)
(front-times-2 "A" 4)
(front-times-2 "Abx" 0)

(defn count-xx-1
  [xs]
  (letfn [(g [ys]
             (if (= [\x \x] (take 2 ys)) 1 0))
          (f [ys]
             (if (empty? ys)
               0
               (+ (g ys) (f (rest ys)))))]
    (f xs)))

(count-xx-1 "abcxx")
(count-xx-1 "xxx")
(count-xx-1 "xxxx")
(count-xx-1 "abc")
(count-xx-1 "Hello there")
(count-xx-1 "Hexxo thexxe")
(count-xx-1 "")
(count-xx-1 "Kittens")
(count-xx-1 "Kittensxxx")

(defn count-xx-2
  [xs]
  (letfn [(g [ys acc]
             (if (= (take 2 ys) [\x \x]) (inc acc) acc))
          (f [ys acc]
             (if (empty? ys)
               acc
               (f (rest ys) (g ys acc))))]
    (f xs 0)))

(count-xx-2 "abcxx")
(count-xx-2 "xxx")
(count-xx-2 "xxxx")
(count-xx-2 "abc")
(count-xx-2 "Hello there")
(count-xx-2 "Hexxo thexxe")
(count-xx-2 "")
(count-xx-2 "Kittens")
(count-xx-2 "Kittensxxx")

(defn string-splosion-1
  [xs]
  (letfn [(g [ys]
             (apply str ys))
          (f [ys]
             (if (empty? ys)
               (g [])
               (g (concat (f (drop-last ys)) ys))))]
    (f xs)))

(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "There")
(string-splosion-1 "Kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")

(defn string-splosion-2
  [xs]
  (letfn [(f [ys acc]
            (if (empty? ys)
              acc
              (f (drop-last ys) (apply str (concat ys acc)))))]
    (f xs "")))

(string-splosion-2 "Code")
(string-splosion-2 "abc")
(string-splosion-2 "ab")
(string-splosion-2 "x")
(string-splosion-2 "fade")
(string-splosion-2 "There")
(string-splosion-2 "Kitten")
(string-splosion-2 "Bye")
(string-splosion-2 "Good")
(string-splosion-2 "Bad")

(defn array-123-1
  [xs]
  (letfn [(f [ys]
             (if (empty? ys)
               false
               (if (= (take 3 ys) '(1 2 3))
                 true
                 (f (rest ys)))))]
    (f xs)))

(array-123-1 [1 1 2 3 1])
(array-123-1 [1 1 2 4 1])
(array-123-1 [1 1 2 1 2 3])
(array-123-1 [1 1 2 1 2 1])
(array-123-1 [1 1 2 1 2 3])
(array-123-1 [1 2 3])
(array-123-1 [1 1 1])
(array-123-1 [1 2])
(array-123-1 [1])
(array-123-1 [])

(defn array-123-2
  [xs]
  (letfn [(f [ys acc]
            (if (= acc 0)
              false
              (if (= (take 3 ys) '(1 2 3))
                true
                (f (rest ys) (dec acc)))))]
    (f xs (count xs))))

(array-123-2 [1 1 2 3 1])
(array-123-2 [1 1 2 4 1])
(array-123-2 [1 1 2 1 2 3])
(array-123-2 [1 1 2 1 2 1])
(array-123-2 [1 1 2 1 2 3])
(array-123-2 [1 2 3])
(array-123-2 [1 1 1])
(array-123-2 [1 2])
(array-123-2 [1])
(array-123-2 [])

(defn string-x-1
  [xs]
  (letfn [(g [ys]
             (apply str ys))
          (f [ys i len]
            (if (= i len)
              (g [])
              (if (not (and (and (> i 0) (< i (- len 1))) (= (subs ys i (inc i)) "x")))
                (g (concat (subs ys i (inc i)) (f ys (inc i) len)))
                (f ys (inc i) len))))]
    (f xs 0 (count xs))))

(string-x-1 "xxHxix")
(string-x-1 "abxxxcd")
(string-x-1 "xabxxxcdx")
(string-x-1 "xKittenx")
(string-x-1 "Hello")
(string-x-1 "xx")
(string-x-1 "x")
(string-x-1 "")

(defn string-x-2
  [xs]
  (letfn [(f [ys i len acc]
            (if (= i len)
              acc
              (f ys (inc i) len 
                 (if (not (and (and (> i 0) (< i (- len 1))) (= (subs ys i (inc i)) "x")))
                (apply str acc (subs ys i (inc i)))
                acc))))]
    (f xs 0 (count xs) "")))

(string-x-2 "xxHxix")
(string-x-2 "abxxxcd")
(string-x-2 "xabxxxcdx")
(string-x-2 "xKittenx")
(string-x-2 "Hello")
(string-x-2 "xx")
(string-x-2 "x")
(string-x-2 "")

(defn alt-pairs-1
  [xs]
  (letfn [(g [ys]
             (apply str ys))
          (f [ys]
            (if (empty? ys)
              (g [])
              (g (concat (take 2 ys) (f (drop 4 ys))))))]
    (f xs)))

(alt-pairs-1 "kitten")
(alt-pairs-1 "Chocolate")
(alt-pairs-1 "CodingHorror")
(alt-pairs-1 "yak")
(alt-pairs-1 "ya")
(alt-pairs-1 "y")
(alt-pairs-1 "")
(alt-pairs-1 "ThisThatTheOther")

(defn alt-pairs-2
  [xs]
  (letfn [(f [ys acc]
            (if (empty? ys)
              acc
              (f (drop 4 ys) (apply str acc (take 2 ys)))))]
    (f xs "")))

(alt-pairs-2 "kitten")
(alt-pairs-2 "Chocolate")
(alt-pairs-2 "CodingHorror")
(alt-pairs-2 "yak")
(alt-pairs-2 "ya")
(alt-pairs-2 "y")
(alt-pairs-2 "")
(alt-pairs-2 "ThisThatTheOther")

(defn string-yak-1
  [xs]
  (letfn [(f [ys]
            (if (empty? ys)
              ""
              (if (not (= (take 3 ys) [\y \a \k]))
                (apply str (apply str (take 1 ys)) (f (drop 1 ys)))
                (f (drop 3 ys)))))]
    (f xs)))

(string-yak-1 "yakpak")
(string-yak-1 "pakyak")
(string-yak-1 "yak123ya")
(string-yak-1 "yak")
(string-yak-1 "yakxxxyak")
(string-yak-1 "HiyakHi")
(string-yak-1 "xxxyakyyyakzzz")

(defn string-yak-2
  [xs]
  (letfn [(f [ys acc]
            (if (empty? ys)
              acc
              (f (if (= (take 3 ys) [\y \a \k])
                   (drop 3 ys)
                   (drop 1 ys)) 
                 (if (= (take 3 ys) [\y \a \k])
                   acc
                   (apply str acc (take 1 ys))))))]
    (f xs "")))

(string-yak-2 "yakpak")
(string-yak-2 "pakyak")
(string-yak-2 "yak123ya")
(string-yak-2 "yak")
(string-yak-2 "yakxxxyak")
(string-yak-2 "HiyakHi")
(string-yak-2 "xxxyakyyyakzzz")

(defn abs [n] (max n (- n)))

(defn has-271
  [xs]
  (letfn [(f [ys]
             (if (< (count ys) 3)
               false
               (if (and (= (nth ys 1) (+ (first ys) 5)) (<= (abs (- (nth ys 2) (dec (first ys)))) 2))
                 true
                 (f (rest ys)))))]
    (f xs)))

(defn has-271-1
  [xs]
  (letfn [(g [ys]
             (and (= (nth ys 1) (+ (first ys) 5)) (<= (abs (- (nth ys 2) (dec (first ys)))) 2)))
          (f [ys]
             (if (< (count ys) 3)
               false
               (or (g ys) (f (rest ys)))))]
    (f xs)))

(has-271-1 [1 2 7 1])
(has-271-1 [1 2 8 1])
(has-271-1 [2 7 1])
(has-271-1 [3 8 2])
(has-271-1 [2 7 3])
(has-271-1 [2 7 4])
(has-271-1 [2 7 -1])
(has-271-1 [2 7 -2])
(has-271-1 [4 5 3 8 0])
(has-271-1 [2 7 5 10 4])
(has-271-1 [2 7 -2 4 9 3])
(has-271-1 [2 7 5 10 1])
(has-271-1 [2 7 -2 4 10 2])
(has-271-1 [1 1 4 9 0])
(has-271-1 [1 1 4 9 4 9 2])

(defn has-271-2
  [xs]
  (letfn [(f [ys acc]
            (if (> acc (- (count ys) 3))
              false
              (if (and (= (nth ys (inc acc)) (+ (nth ys acc) 5)) (<= (abs (- (nth ys (+ acc 2)) (dec (nth ys acc)))) 2))
                true
                (f ys (inc acc)))))]
    (f xs 0)))

(has-271-2 [1 2 7 1])
(has-271-2 [1 2 8 1])
(has-271-2 [2 7 1])
(has-271-2 [3 8 2])
(has-271-2 [2 7 3])
(has-271-2 [2 7 4])
(has-271-2 [2 7 -1])
(has-271-2 [2 7 -2])
(has-271-2 [4 5 3 8 0])
(has-271-2 [2 7 5 10 4])
(has-271-2 [2 7 -2 4 9 3])
(has-271-2 [2 7 5 10 1])
(has-271-2 [2 7 -2 4 10 2])
(has-271-2 [1 1 4 9 0])
(has-271-2 [1 1 4 9 4 9 2])